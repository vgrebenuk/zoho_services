lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)

require "zoho_services/version"

Gem::Specification.new do |spec|
  spec.name          = "zoho_services"
  spec.version       = ZohoServices::VERSION
  spec.authors       = ["Vladimir Grebenuk"]
  spec.email         = ["vgrebenuk@positrace.com"]

  spec.summary       = 'Zoho services'
  spec.description   = 'Working with zoho API V2'
  spec.homepage      = 'http://www.positrace.com'

  spec.files = Dir['{lib}/**/**/**/**/*'] + %w[Rakefile README.md]
  spec.test_files = Dir['spec/**/*']
  spec.require_paths = ["lib"]

  spec.add_development_dependency 'bundler', '~> 2.0'
  spec.add_development_dependency 'pry'
  spec.add_development_dependency 'rake', '~> 10.0'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'rspec-retry'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'shoulda'
  spec.add_development_dependency 'shoulda-callback-matchers', '~> 1.1.1'
  spec.add_development_dependency 'shoulda-matchers', '~> 3.1'
  spec.add_development_dependency 'simplecov'
  spec.add_development_dependency 'webmock'
end
