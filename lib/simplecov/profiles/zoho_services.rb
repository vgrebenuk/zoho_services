require 'simplecov'

SimpleCov.profiles.define 'zoho_services' do
  load_profile 'test_frameworks'

  add_filter %r{^/config/}
  add_filter '/lib/simplecov/'
  add_filter '/lib/zoho_services/version.rb'

  add_group 'Libraries', 'lib/'

  track_files '{lib}/**/*.rb'
end