require_relative 'crm/configuration'

module ZohoServices
  module Crm
    class << self
      attr_reader :configuration

      def configure
        @configuration ||= Configuration.new
        yield(configuration) if block_given?
      end
    end
  end
end
