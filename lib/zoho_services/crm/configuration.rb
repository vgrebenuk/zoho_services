module ZohoServices
  module Crm
    class Configuration
      attr_accessor :api_key

      HOST = 'https://www.zohoapis.com/crm/v2'

      def initialize
        @api_key = nil
      end

      def host
        HOST
      end

    end

  end
end