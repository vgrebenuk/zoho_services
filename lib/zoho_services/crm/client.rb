module ZohoServices
  module Crm
    class Client
      include HTTMultiParty

      attr_reader :crm_module, :base_uri

      def initialize(crm_module)
        @crm_module = crm_module
        @base_uri = [host, crm_module].join('/')
      end

      def put(uri, options)
        self.class.put(
          [base_uri, uri].join('/'),
          query: options.to_json,
          headers: headers
        )
      end

      private

      def api_key
        Crm.configuration.api_key
      end

      def host
        Crm.configuration.host
      end

      def headers
        {
          'Authorization' => api_key,
          'Content-Type' => 'application/json'
        }
      end

    end
  end
end
