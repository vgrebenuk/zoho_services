require_relative 'base_entity'

module ZohoServices
  module Crm
    module Entities
      class RecordEntity < BaseEntity

        class << self
          def update_related(record_id, relation_options)
            related_module = relation_options[:related_module]
            related_id = relation_options[:related_id]

            send_request do
              client.put(
                [record_id, related_module, related_id].join('/'),
                data: relation_options[:data]
              )
            end
          end

          private

          def send_request
            response = yield

            response.success? ? handle_success(response) : handle_error(response)
          end

          def handle_success(response)
            JSON.parse(response.body).to_hash
          end

          def handle_error(response)
            raise Errors::ResponseError, response
          end
        end

      end
    end
  end
end