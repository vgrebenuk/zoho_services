module ZohoServices
  module Crm
    module Entities
      class BaseEntity

        class << self
          def crm_module
            name.demodulize.pluralize
          end

          def client
            Client.new(crm_module)
          end
        end
      end
    end
  end
end