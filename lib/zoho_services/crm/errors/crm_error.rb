module ZohoServices
  module Crm
    module Errors
      class ResponseError < StandardError
        attr_reader :response

        def initialize(response)
          @response = response
          super
        end
      end

    end
  end
end