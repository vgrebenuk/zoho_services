require "bundler/setup"
require "zoho_services"
require 'shoulda/matchers'
require 'rspec/retry'

ENV['RAILS_ENV'] ||= 'test'

require File.expand_path('../../lib/simplecov/profiles/zoho_services', __FILE__)

if ENV['CIRCLE_ARTIFACTS']
  dir = File.join(ENV['CIRCLE_ARTIFACTS'], 'coverage')
  SimpleCov.coverage_dir(dir)
end

SimpleCov.start 'zoho_services'

require 'pry'
# binding.pry
Dir.glob('./spec/support/**/*.rb').each { |f| require f }

RSpec.configure do |config|
  config.order = 'random'
  config.default_retry_count = 3

  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end
end
