require 'spec_helper'
require 'webmock/rspec'

module ZohoServices
  module Crm

    describe Client, type: :model do

      let(:crm_module) { 'Leads' }
      let(:api_key) { Crm.configuration.api_key }
      let(:headers) do
        {
          'Authorization' => api_key,
          'Content-Type' => 'application/json'
        }
      end
      let(:client) { described_class.new(crm_module) }

      describe 'initialize' do
        it 'fill crm_module' do
          expect(client.crm_module).to eq crm_module
        end

        it 'set base_uri' do
          base_uri = [Crm.configuration.host, crm_module].join('/')
          expect(client.base_uri).to eq base_uri
        end
      end

      describe 'put' do
        let(:uri) { 'get_action' }
        let(:options) { { test: :test } }
        let(:body) { { body: :body } }

        before(:each) do
          stub_request(:put, [Crm.configuration.host, crm_module, uri].join('/')).
              with(body: options.to_json, headers: headers).
              to_return(
                 headers: { 'Content-Type' =>'application/json' },
                 body: body.to_json,
                 status: 200
              )
        end

        it 'send get request' do
          response = client.put(uri, options)
          expect(response.success?).to eq true
        end
      end
    end
  end
end