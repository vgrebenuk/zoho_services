require 'spec_helper'
require 'webmock/rspec'

module ZohoServices
  module Crm
    module Entities

      describe Lead, type: :model do
        include_examples 'zoho_service_crm_entity_record'

      end

    end
  end
end