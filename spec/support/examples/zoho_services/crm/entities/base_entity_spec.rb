require 'spec_helper'

module ZohoServices
  module Crm
    module Entities

      RSpec.shared_examples_for 'zoho_service_crm_entity_base' do

        describe 'initialize' do

          let(:client) { described_class.client }

          it 'initialize client' do
            expect(client).to be_an_instance_of(Crm::Client)
          end

          it 'client crm_module same as instance class' do
            expect(client.crm_module).to eq described_class.crm_module
          end
        end

        describe 'crm_module' do
          let(:crm_module) { described_class.name.demodulize.pluralize }

          it 'crm_module according described class' do
            expect(described_class.crm_module).to eq crm_module
          end
        end

      end
    end
  end
end