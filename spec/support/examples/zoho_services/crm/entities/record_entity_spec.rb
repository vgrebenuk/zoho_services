require 'spec_helper'

module ZohoServices
  module Crm
    module Entities

      RSpec.shared_examples_for 'zoho_service_crm_entity_record' do
        include_examples 'zoho_service_crm_entity_base'

        let(:host) { Crm.configuration.host }
        let(:api_key) { Crm.configuration.api_key }
        let(:headers) do
          {
            'Authorization' => api_key,
            'Content-Type' => 'application/json'
          }
        end

        describe 'update_related' do
          let(:record_id) { Faker::Number.number }

          let(:related_id) { Faker::Number.number }
          let(:related_module) { 'Campaing' }
          let(:data) { { data: :test_data} }

          let(:related_options) do
            {
              related_id: related_id,
              related_module: related_module,
              data: data
            }
          end

          let(:uri) do
            [record_id, related_module, related_id].join('/')
          end

          let(:body) { { body: :body } }

          let(:response_status) { 200 }

          let(:client) { described_class.client }

          before(:each) do
            url = [host, described_class.crm_module, uri].join('/')
            stub_request(:put, url)
              .with(
                body: { data: related_options[:data] }.to_json,
                headers: headers
              )
              .to_return(
                headers: { 'Content-Type' =>'application/json' },
                body: body.to_json,
                status: response_status
              )
          end

          it 'call client put' do
            allow(described_class).to receive(:client).and_return(client)
            expect(client).to(
              receive(:put)
              .with(uri, { data: related_options[:data] })
              .and_call_original
            )
            described_class.update_related(record_id, related_options)
          end

          context 'when success response' do
            it 'call handle success' do
              expect(described_class)
                .to receive(:handle_success).and_call_original
              described_class.update_related(record_id, related_options)
            end
          end

          context 'when error response' do
            let(:response_status) { 500 }

            it 'call handle error' do
              expect(described_class).to receive(:handle_error)
              described_class.update_related(record_id, related_options)
            end
          end
        end

      end
    end
  end
end
